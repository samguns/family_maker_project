from setuptools import setup

package_name = 'classification_python'

setup(
    name=package_name,
    version='0.0.1',
    packages=[
        package_name,
        package_name + '/camera_stream',
        package_name + '/torch_model'
    ],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Wang Gang',
    maintainer_email='wang.vai@gmail.com',
    description='TODO: Package description',
    license='BSD',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'classification_python_node = classification_python.classification_python_node:main'
        ],
    },
)
