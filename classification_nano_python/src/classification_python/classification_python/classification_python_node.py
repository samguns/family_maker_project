import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool

from .camera_stream.camera_stream import CameraStream
from .torch_model.torch_model import TorchModel
import cv2


class ClassificationNode(Node):
    def __init__(self):
        super().__init__('classification_node')
        self.publisher = self.create_publisher(
            Bool,
            'gate_switch',
            10
        )

        self.camera_stream = CameraStream()
        self.inference_model = TorchModel()

        timer_interval = 0.05  # 50ms, that is 20Hz, 20 FPS video sampling
        self.timer = self.create_timer(
            timer_interval,
            self.timer_callback
        )

    def timer_callback(self):
        stream_frame, stream_rgb_frame = self.camera_stream.get_frame()
        if stream_rgb_frame is None:
            return

        wear_mask, confidence = \
            self.inference_model.detect_mask(stream_rgb_frame)

        msg = Bool()

        if wear_mask:
            msg.data = True
            display_text = 'Mask: %.2f' % round(confidence, 2)
            color = (0, 255, 0)
        else:
            msg.data = False
            display_text = 'No Mask: %.2f' % round(confidence, 2)
            color = (0, 0, 255)

        self.publisher.publish(msg)

        cv2.putText(stream_frame, display_text, (180, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 1,
                    color, 2)
        cv2.imshow('Detection', stream_frame)
        cv2.waitKey(1)

        return


def main():
    rclpy.init()
    classification_node = ClassificationNode()
    rclpy.spin(classification_node)

    classification_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
