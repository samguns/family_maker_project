import torch
import torch.nn.functional as F
from torchvision import transforms, models
from PIL import Image


class TorchModel:
    def __init__(self):
        self.eval_transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ])

        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )

        self.model = torch.load('model_conv.pth')
        self.model.to(self.device)
        self.model.eval()

        return

    def detect_mask(self, input_image):
        """
        :param input_image: RAW RGB image
        :return: True if it detects a mask with confidence
                 False if it detects no mask and with its confidence
        """
        img = Image.fromarray(input_image)
        input_tensor = self.eval_transform(img).to(self.device)

        outputs = self.model(input_tensor[None, ...])
        probs = F.softmax(outputs, dim=1).detach().cpu().numpy().flatten()

        if probs[0] > probs[1]:
            return True, probs[0]

        return False, probs[1]
