//
// Created by Wang Gang on 2020/3/24.
//

#include <chrono>

#include "ClassificationNode.h"

using namespace std::chrono_literals;


ClassificationNode::ClassificationNode(const std::string& model_filename)
 : Node("classification_node") {
  this->_publisher =
      this->create_publisher<std_msgs::msg::Bool>("gate_switch", 10);

  this->_timer =
      this->create_wall_timer(
          50ms,
          std::bind(&ClassificationNode::timer_callback, this)
          );

  this->_torch_model.LoadModel(model_filename);

  return;
}

ClassificationNode::~ClassificationNode() {
}


void ClassificationNode::timer_callback() {
  std_msgs::msg::Bool msg = std_msgs::msg::Bool();

  cv::Mat frame;
  bool ret = this->_camera.ReadFrame(&frame);
  if (!ret) {
    msg.data = false;
    RCLCPP_INFO(this->get_logger(), "Publishing false");
    this->_publisher->publish(msg);

    return;
  }

//    cv::imshow("web", frame);

  msg.data = true;
  this->_publisher->publish(msg);
  RCLCPP_INFO(this->get_logger(), "Publishing true");

  return;
}
