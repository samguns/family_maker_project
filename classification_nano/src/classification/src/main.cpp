//
// Created by Wang Gang on 2020/3/24.
//

#include <memory>
#include <rclcpp/rclcpp.hpp>

#include "ClassificationNode.h"


int main(int argc, char *argv[]) {
  std::string modle_path(argv[1]);

  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ClassificationNode>(modle_path));
  rclcpp::shutdown();

  return 0;
}

