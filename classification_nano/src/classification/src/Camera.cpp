//
// Created by Wang Gang on 2020/3/26.
//

#include "Camera.h"

Camera::Camera() {
  this->_video_capture = cv::VideoCapture(0);
}

Camera::~Camera() {
  this->_video_capture.release();
}


bool Camera::ReadFrame(cv::Mat *pFrame) {
  cv::Mat frame;
  bool ret = this->_video_capture.read(frame);
  if (!ret) {
    return ret;
  }

  frame.copyTo(*pFrame);
  return true;
}
