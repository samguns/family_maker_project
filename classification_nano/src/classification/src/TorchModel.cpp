//
// Created by Wang Gang on 2020/3/24.
//

#include <iostream>
#include "TorchModel.h"


TorchModel::TorchModel() {
  if (torch::cuda::is_available()) {
    this->_device_type = torch::kCUDA;
  } else {
    this->_device_type = torch::kCPU;
  }

  return;
}


bool TorchModel::LoadModel(const std::string& model_filename) {
  try {
    this->_module = torch::jit::load(model_filename.c_str());
  } catch (const c10::Error& e) {
    std::cerr << "Error loading the model\n";
    return false;
  }

  this->_module.to(this->_device_type);

  return true;
}
