//
// Created by Wang Gang on 2020/3/24.
//

#ifndef CLASSIFICATION_TORCHMODEL_H
#define CLASSIFICATION_TORCHMODEL_H

#include <torch/torch.h>
#include <torch/script.h>

class TorchModel {
 public:
  TorchModel();

  bool LoadModel(const std::string& model_filename);
 private:
  torch::jit::script::Module _module;
  torch::DeviceType _device_type;
};


#endif //CLASSIFICATION_TORCHMODEL_H
