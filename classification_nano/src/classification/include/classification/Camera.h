//
// Created by Wang Gang on 2020/3/26.
//

#ifndef CLASSIFICATION_CLASSIFICATION_NANO_SRC_CLASSIFICATION_SRC_CAMERA_H_
#define CLASSIFICATION_CLASSIFICATION_NANO_SRC_CLASSIFICATION_SRC_CAMERA_H_

#include <opencv2/opencv.hpp>

class Camera {
 public:
  Camera();
  ~Camera();

  bool ReadFrame(cv::Mat*);

 private:
  cv::VideoCapture _video_capture;
};

#endif //CLASSIFICATION_CLASSIFICATION_NANO_SRC_CLASSIFICATION_SRC_CAMERA_H_
