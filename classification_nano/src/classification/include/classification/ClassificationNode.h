//
// Created by Wang Gang on 2020/3/24.
//

#ifndef FAMILY_MAKER_PROJECT_CLASSIFICATIONNODE_H
#define FAMILY_MAKER_PROJECT_CLASSIFICATIONNODE_H

#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/bool.hpp>

#include "Camera.h"
#include "TorchModel.h"


class ClassificationNode: public rclcpp::Node {
 public:
  ClassificationNode(const std::string& model_filename);
  ~ClassificationNode();

 private:
  rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr _publisher;
  rclcpp::TimerBase::SharedPtr _timer;
  TorchModel _torch_model;
  Camera _camera;

  void timer_callback();
};


#endif //FAMILY_MAKER_PROJECT_CLASSIFICATIONNODE_H
