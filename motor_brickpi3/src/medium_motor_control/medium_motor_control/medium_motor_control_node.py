import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
from brickpi3 import BrickPi3

HALF_POWER = 50
FULL_POWER = 100
DEGREES_PER_SECOND_300 = 300
DEGREES_PER_SECOND_100 = 100

GATE_PASS_DEGREES = 90
GATE_SHUT_DEGREES = 0


class GateSwitchSubscriber(Node):
    def __init__(self):
        super().__init__('motor_control_gate_switch_subscriber')
        self.subscription = self.create_subscription(
            Bool,
            'gate_switch',
            self.listener_callback,
            10
        )

        self.counter = 0
        self.open_gate = False
        self.close_gate = True
        self.counter_threshold = 8
        self.bp = BrickPi3()
        self.motor_port = self.bp.PORT_B
        self.is_motor_initialized = False

        self.initialize_brickpi()
        return

    def __del__(self):
        if self.is_motor_initialized:
            self.bp.reset_all()

        return

    def initialize_brickpi(self):
        try:
            self.bp.reset_motor_encoder(self.motor_port)
            self.bp.set_motor_power(self.motor_port,
                                    self.bp.MOTOR_FLOAT)
            self.bp.set_motor_limits(self.motor_port,
                                     HALF_POWER,
                                     DEGREES_PER_SECOND_300)
        except IOError as err:
            print(err)

        self.is_motor_initialized = True
        return

    def listener_callback(self, msg):
        if not self.is_motor_initialized:
            return

        if msg.data:
            # If gate is closed previously, start jitter filter counter
            if self.close_gate:
                self.counter = 0
                self.close_gate = False

            # If jitter threshold meets and gate is not opened
            if self.counter > self.counter_threshold and not self.open_gate:
                self.bp.set_motor_position(self.motor_port, GATE_PASS_DEGREES)
                self.open_gate = True
        else:
            # If gate is opened previously, start jitter filter counter
            if self.open_gate:
                self.counter = 0
                self.open_gate = False

            # If jitter threshold meets and gate is not closed
            if self.counter > self.counter_threshold and not self.close_gate:
                self.bp.set_motor_position(self.motor_port, GATE_SHUT_DEGREES)
                self.close_gate = True

        self.counter += 1
        if self.counter > 255:
            self.counter = 255

        return


def main():
    rclpy.init()
    gate_switch_subscriber = GateSwitchSubscriber()
    rclpy.spin(gate_switch_subscriber)

    return


if __name__ == '__main__':
    main()
