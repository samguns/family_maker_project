## Build classification_nano

```shell script
cd classification_nano
source /opt/ros/dashing/setup.bash
export CMAKE_PREFIX_PATH=/home/sam/anaconda3/envs/torch/lib/python3.7/site-packages/torch:/home/sam/anaconda3/envs/torch
colcon build
```