import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
from sense_hat import SenseHat

TEXT_SCROLL_PER_SECOND = 0.1


class GateSwitchSubscriber(Node):
    def __init__(self):
        super().__init__('led_control_gate_switch_subscriber')
        self.subscription = self.create_subscription(
            Bool,
            'gate_switch',
            self.listener_callback,
            10
        )

        self.counter = 0
        self.open_gate = False
        self.close_gate = True
        self.counter_threshold = 8
        self.is_sense_hat_initialized = False
        self.stop_mark = None
        self.go_mark = None
        try:
            self.sense_hat = SenseHat()
        except OSError as err:
            print(err)

        self.initialize_sense_hat()
        if self.is_sense_hat_initialized:
            self.sense_hat.show_message('READY', TEXT_SCROLL_PER_SECOND)
        return

    def initialize_sense_hat(self):
        g = [0, 255, 0]
        r = [255, 0, 0]
        o = [0, 0, 0]

        self.stop_mark = [
            r, o, o, o, o, o, o, r,
            o, r, o, o, o, o, r, o,
            o, o, r, o, o, r, o, o,
            o, o, o, r, r, o, o, o,
            o, o, o, r, r, o, o, o,
            o, o, r, o, o, r, o, o,
            o, r, o, o, o, o, r, o,
            r, o, o, o, o, o, o, r
        ]

        self.go_mark = [
            o, o, o, g, g, o, o, o,
            o, o, g, g, g, g, o, o,
            o, g, o, g, g, o, g, o,
            g, o, o, g, g, o, o, g,
            o, o, o, g, g, o, o, o,
            o, o, o, g, g, o, o, o,
            o, o, o, g, g, o, o, o,
            o, o, o, g, g, o, o, o
        ]

        self.is_sense_hat_initialized = True
        return

    def listener_callback(self, msg):
        if msg.data:
            # If gate is closed previously, start jitter filter counter
            if self.close_gate:
                self.counter = 0
                self.close_gate = False

            # If jitter threshold meets and gate is not opened
            if self.counter > self.counter_threshold and not self.open_gate:
                self.sense_hat.set_pixels(self.go_mark)
                self.open_gate = True
        else:
            # If gate is opened previously, start jitter filter counter
            if self.open_gate:
                self.counter = 0
                self.open_gate = False

            # If jitter threshold meets and gate is not closed
            if self.counter > self.counter_threshold and not self.close_gate:
                self.sense_hat.set_pixels(self.stop_mark)
                self.close_gate = True

        self.counter += 1
        if self.counter > 255:
            self.counter = 255

        return


def main():
    rclpy.init()
    gate_switch_subscriber = GateSwitchSubscriber()
    rclpy.spin(gate_switch_subscriber)

    return
